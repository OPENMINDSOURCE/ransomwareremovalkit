
var gfeedfetcher_loading_image="/scripts/web/js/indicator.gif"

google.load("feeds", "1") 

function gfeedfetcher(divid, divClass, linktarget){
    this.linktarget = linktarget || "";
	this.feedlabels = [];
	this.feedurls = [];
	this.feeds = [];
	this.feedsfetched = 0;
	this.feedlimit = 5;
	this.showoptions = "";
	this.sortstring = "date";
	if (document.getElementById(divid) == null) {
	    document.write('<div id="' + divid + '" class="' + divClass + '"></div>');
	}
	this.feedcontainer = document.getElementById(divid);
	this.itemcontainer = "<li>";
}

gfeedfetcher.prototype.addFeed=function(label, url){
	this.feedlabels[this.feedlabels.length]=label
	this.feedurls[this.feedurls.length]=url
}

gfeedfetcher.prototype.filterfeed=function(feedlimit, sortstr){
	this.feedlimit=feedlimit
	if (typeof sortstr!="undefined")
	this.sortstring=sortstr
}

gfeedfetcher.prototype.displayoptions=function(parts){
	this.showoptions=parts 
}

gfeedfetcher.prototype.setentrycontainer=function(containerstr){  
this.itemcontainer="<"+containerstr.toLowerCase()+">"
}

gfeedfetcher.prototype.init=function(){
	this.feedsfetched=0 
	this.feeds=[] 
	this.feedcontainer.innerHTML='<p><img src="'+gfeedfetcher_loading_image+'" /> Retrieving Data...</p>'
	var displayer=this
	for (var i=0; i<this.feedurls.length; i++){ 
		var feedpointer=new google.feeds.Feed(this.feedurls[i])
		var items_to_show=(this.feedlimit<=this.feedurls.length)? 1 : Math.floor(this.feedlimit/this.feedurls.length) 
		if (this.feedlimit%this.feedurls.length>0 && this.feedlimit>this.feedurls.length && i==this.feedurls.length-1)
			items_to_show+=(this.feedlimit%this.feedurls.length) 
		feedpointer.setNumEntries(items_to_show) 
		feedpointer.load(function(label){
			return function(r){
				displayer._fetch_data_as_array(r, label)
			}
		}(this.feedlabels[i])) 
	}
}


gfeedfetcher._formatdate=function(datestr, showoptions){
	var itemdate=new Date(datestr)
	var parseddate=(showoptions.indexOf("datetime")!=-1)? itemdate.toLocaleString() : (showoptions.indexOf("date")!=-1)? itemdate.toLocaleDateString() : (showoptions.indexOf("time")!=-1)? itemdate.toLocaleTimeString() : ""
	return "<span class='datefield'>"+parseddate+"</span>"
}

gfeedfetcher._sortarray=function(arr, sortstr){
	var sortstr=(sortstr=="label")? "ddlabel" : sortstr 
	if (sortstr=="title" || sortstr=="ddlabel"){ 
		arr.sort(function(a,b){
		var fielda=a[sortstr].toLowerCase()
		var fieldb=b[sortstr].toLowerCase()
		return (fielda<fieldb)? -1 : (fielda>fieldb)? 1 : 0
		})
	}
	else{ 
		try{
			arr.sort(function(a,b){return new Date(b.publishedDate)-new Date(a.publishedDate)})
		}
		catch(err){}
	}
}

gfeedfetcher.prototype._fetch_data_as_array=function(result, ddlabel){	
	var thisfeed=(!result.error)? result.feed.entries : "" 
	if (thisfeed==""){
		alert("Some blog posts could not be loaded: "+result.error.message)
	}
	for (var i=0; i<thisfeed.length; i++){ 
		result.feed.entries[i].ddlabel=ddlabel 
	}
	this.feeds=this.feeds.concat(thisfeed) 
	this._signaldownloadcomplete() 
}

gfeedfetcher.prototype._signaldownloadcomplete=function(){
	this.feedsfetched+=1
	if (this.feedsfetched==this.feedurls.length) 
		this._displayresult(this.feeds) 
}


gfeedfetcher.prototype._displayresult=function(feeds){
	var rssoutput=(this.itemcontainer=="<li>")? "<ul class=\"red-arrow\">\n" : ""
	gfeedfetcher._sortarray(feeds, this.sortstring)
	for (var i=0; i<feeds.length; i++){
		var itemtitle="<a rel=\"nofollow\" href=\"" + feeds[i].link + "\" target=\"" + this.linktarget + "\" class=\"titlefield\">" + feeds[i].title + "</a>"
		var itemlabel=/label/i.test(this.showoptions)? '<span class="labelfield">['+this.feeds[i].ddlabel+']</span>' : " "
		var itemdate=gfeedfetcher._formatdate(feeds[i].publishedDate, this.showoptions)
		var itemdescription=/description/i.test(this.showoptions)? "<br />"+feeds[i].content : /snippet/i.test(this.showoptions)? "<br />"+feeds[i].contentSnippet  : ""
		rssoutput+=this.itemcontainer + itemtitle + " " + itemlabel + " " + itemdate + "\n" + itemdescription + this.itemcontainer.replace("<", "</") + "\n\n"
	}
	rssoutput+=(this.itemcontainer=="<li>")? "</ul>" : ""
	this.feedcontainer.innerHTML=rssoutput
}







var gfeedfetcher2_loading_image="/scripts/web/js/indicator.gif"

google.load("feeds", "1") 

function gfeedfetcher2(divid, divClass, linktarget){
	this.linktarget=linktarget || "" 
	this.feedlabels=[] 
	this.feedurls=[]
	this.feeds=[] 
	this.feedsfetched=0 
	this.feedlimit=5
	this.showoptions="" 
	this.sortstring="date"
	document.write('<div id="' + divid + '" class="' + divClass + '"></div>');
	this.feedcontainer=document.getElementById(divid)
	this.itemcontainer="<li>" 
}

gfeedfetcher2.prototype.addFeed=function(label, url){
	this.feedlabels[this.feedlabels.length]=label
	this.feedurls[this.feedurls.length]=url
	//this.feedlabels[this.feedlabels.length]=label2
	//this.feedurls[this.feedurls.length]=url2
}

gfeedfetcher2.prototype.filterfeed=function(feedlimit, sortstr){
	this.feedlimit=feedlimit
	if (typeof sortstr!="undefined")
	this.sortstring=sortstr
}

gfeedfetcher2.prototype.displayoptions=function(parts){
	this.showoptions=parts 
}

gfeedfetcher2.prototype.setentrycontainer=function(containerstr){  
	this.itemcontainer="<"+containerstr.toLowerCase()+">"
}

gfeedfetcher2.prototype.init=function(){
	this.feedsfetched=0 
	this.feeds=[] 
	this.feedcontainer.innerHTML='<p><img src="'+gfeedfetcher2_loading_image+'" /> Retrieving Data...</p>'
	var displayer=this
	for (var i=0; i<this.feedurls.length; i++){ 
		var feedpointer=new google.feeds.Feed(this.feedurls[i])
		var items_to_show=(this.feedlimit<=this.feedurls.length)? 1 : Math.floor(this.feedlimit/this.feedurls.length) 
		if (this.feedlimit%this.feedurls.length>0 && this.feedlimit>this.feedurls.length && i==this.feedurls.length-1)
			items_to_show+=(this.feedlimit%this.feedurls.length) 
		feedpointer.setNumEntries(items_to_show) 
		feedpointer.load(function(label){
			return function(r){
				displayer._fetch_data_as_array(r, label)
			}
		}(this.feedlabels[i])) 
	}
}


gfeedfetcher2._formatdate=function(datestr, showoptions){
	var itemdate=new Date(datestr)
	var parseddate=(showoptions.indexOf("datetime")!=-1)? itemdate.toLocaleString() : (showoptions.indexOf("date")!=-1)? itemdate.toLocaleDateString() : (showoptions.indexOf("time")!=-1)? itemdate.toLocaleTimeString() : ""
	return "<span class='datefield'>"+parseddate+"</span>"
}

gfeedfetcher2._sortarray=function(arr, sortstr){
	var sortstr=(sortstr=="label")? "ddlabel" : sortstr 
	if (sortstr=="title" || sortstr=="ddlabel"){ 
		arr.sort(function(a,b){
		var fielda=a[sortstr].toLowerCase()
		var fieldb=b[sortstr].toLowerCase()
		return (fielda<fieldb)? -1 : (fielda>fieldb)? 1 : 0
		})
	}
	else{ 
		try{
			arr.sort(function(a,b){return new Date(b.publishedDate)-new Date(a.publishedDate)})
		}
		catch(err){}
	}
}

gfeedfetcher2.prototype._fetch_data_as_array=function(result, ddlabel){	
	var thisfeed=(!result.error)? result.feed.entries : "" 
	if (thisfeed==""){
		alert("Some blog posts could not be loaded: "+result.error.message)
	}
	for (var i=0; i<thisfeed.length; i++){ 
		result.feed.entries[i].ddlabel=ddlabel 
	}
	this.feeds=this.feeds.concat(thisfeed) 
	this._signaldownloadcomplete() 
}

gfeedfetcher2.prototype._signaldownloadcomplete=function(){
	this.feedsfetched+=1
	if (this.feedsfetched==this.feedurls.length) 
		this._displayresult(this.feeds) 
}


gfeedfetcher2.prototype._displayresult=function(feeds){
	var rssoutput=(this.itemcontainer=="<li>")? "<ul class=\"red-arrow\">\n" : ""
	gfeedfetcher2._sortarray(feeds, this.sortstring)
	for (var i=0; i<feeds.length; i++){
		var itemtitle="<a rel=\"nofollow\" href=\"" + feeds[i].link + "\" target=\"" + this.linktarget + "\" class=\"titlefield\">" + feeds[i].title + "</a>"
		var itemlabel=/label/i.test(this.showoptions)? '<span class="labelfield">['+this.feeds[i].ddlabel+']</span>' : " "
		var itemdate=gfeedfetcher2._formatdate(feeds[i].publishedDate, this.showoptions)
		var itemdescription=/description/i.test(this.showoptions)? "<br />"+feeds[i].content : /snippet/i.test(this.showoptions)? "<br />"+feeds[i].contentSnippet  : ""
		rssoutput+=this.itemcontainer + itemtitle + " " + itemlabel + " " + itemdate + "\n" + itemdescription + this.itemcontainer.replace("<", "</") + "\n\n"
	}
	rssoutput+=(this.itemcontainer=="<li>")? "</ul>" : ""
	this.feedcontainer.innerHTML=rssoutput
}