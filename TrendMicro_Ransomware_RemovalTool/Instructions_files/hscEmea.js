$(document).ready(function(){
  var userLang = navigator.language || navigator.userLanguage; 
  $(".show-alert-in-anz").closest('div[class^="alert"]').hide();
  $(".show-alert-in-hk").closest('div[class^="alert"]').hide();
  var isEmea=isEmeaLocale(); 
  if(isEmea) {
    $(".hide-parentrow-in-emea").closest('div[class^="row"]').hide();
     $(".hide-parentcol-in-emea").closest('div[class^="col"]').hide();
    $(".hide-in-emea").hide();
    
    $(".show-parentrow-in-emea").closest('div[class^="row"]').show();
    $(".show-in-emea").show(); 
    updateGlobalNav();
    if(isContactPage()) {
      var curlocale=getCurrentLocale();
      if(noPhoneSupport()==false) {      
        var divCont = $("<div />", { "class": 'lwi iconSupportPhone' });      
        var h = '<h6><b>Call Standard Support</b></h6>';
        h+='<p>Talk to a Support Engineer. Availability may vary.</p>';         
        divCont.append(h);   
        
        
        var emeaNumdiv= $("<div />", {"id": 'emeaContactDiv', "class": 'emea-number' });
        emeaNumdiv.append($("<h6 />", { "id": 'curCountry' }));
        emeaNumdiv.append($("<p />", { "class": 'phone' }));
        emeaNumdiv.append($("<p />", { "class": 'time' }).html('Monday to Friday 8:00 – 17:30 GMT'));        
        divCont.append(emeaNumdiv);
        
        var emeaMsgDiv=$("<div />", {"id": 'emeaPhoneMsgDiv', "class": 'msg' });
        divCont.append(emeaMsgDiv);
        
        var emeaSeldiv= $("<div />", {"class": 'col-md-6 margin-top-2 padding-left-0' });
        emeaSeldiv.append('<span class="small bold">Not your location?</span>');
        emeaSeldiv.append('<select id="selLocale" class="form-control"></select>'); 
        //emeaSeldiv.css({"padding-left": '0px';});
        divCont.append(emeaSeldiv);    
     
        var rowDiv = $("<div />", { "class": 'row content-block' }); 
        
        
        var colDiv = $("<div />", { "class": 'col-xs-12 col-md-6' }); 
        colDiv.append(divCont);
        rowDiv.append(colDiv);    
        
        colDiv='<div class="col-md-6 col-xs-12"> <div class=" lwi iconPremiumService"><h6><b>Call Premium Support</b></h6><p>Call the 24/7 support hotline for Premium Service subscribers.</p>';
        colDiv += '<div class="acc btn btn-sm"><a id="btnPremiumEmea" class="collapsed" data-parent="_self" data-target="#PremiumNumbersEmea" data-toggle="collapse">View Premium Support Number</a></div>';
        colDiv += '<div class="uk emea-number collapse" id="PremiumNumbersEmea" style="height: 20px;"><h6>United Kingdom</h6><p class="phone">808-189-1455</p></div></div></div>';
  
        rowDiv.append(colDiv);        
        $("div.col-md-12").append(rowDiv);
        
        createEmeaLocaleOptions('selLocale');
        
        $("#selLocale").change(function(){
            var l= $("#selLocale").val();
            displayContactInfo(l);              
        });
        
        //$("#selLocale").val(curlocale);
        displayContactInfo(curlocale); 
      } //noPhoneSupport close         
      
      var phoneMsg='';
      var emailMsg='';
      var popMsg='';
      switch (curlocale) {
        case 'fr-fr':
          phoneMsg="Veuillez noter que l&#39;assistance technique est disponible uniquement <a href='#emeaPopDiv'>en anglais</a>. Si vous préférez nous contacter par e-mail (en anglais uniquement), veuillez cliquer sur « Send an Email ».";
          emailMsg="Veuillez noter que l&#39;assistance technique est disponible uniquement <a href='#emeaPopDiv'>en anglais</a>. Veuillez fournir une description détaillée de votre problème technique en anglais.";
          popMsg="Si vous ne souhaitez pas communiquer en anglais, vous pouvez utiliser notre service de messagerie instantanée.  Ce service réalise une traduction automatique de vos communications. Aussi, veuillez noter que la qualité de ces traductions est aléatoire. ";
          break;
        case 'it-it':
          phoneMsg="L’assistenza tecnica viene fornita solo in <a href='#emeaPopDiv'>inglese</a>. Se desiderate contattarci via e-mail (solo inglese), cliccate l’opzione che vi consente di inviarci un’e-mail.";
          emailMsg="L’assistenza tecnica viene fornita solo in <a href='#emeaPopDiv'>inglese</a>. Fornite una descrizione dettagliata del problema tecnico in inglese.";
          popMsg="Se non parlate inglese, utilizzate il nostro servizio di chat.  Questo servizio viene fornito con l’ausilio di traduzioni automatiche, pertanto la qualità delle traduzioni potrebbe variare. ";
          break;
        case 'de-de':
          phoneMsg="Bitte beachten Sie, dass unser technischer Support nur in <a href='#emeaPopDiv'>englischer</a> Sprache angeboten wird. Wenn Sie uns per E-Mail kontaktieren möchten (nur auf Englisch), klicken Sie auf „Send an Email“. ";
          emailMsg="Bitte beachten Sie, dass unser technischer Support nur in <a href='#emeaPopDiv'>englischer</a> Sprache angeboten wird. Bitte erläutern Sie Ihr technisches Problem möglichst ausführlich auf Englisch.";
          popMsg="Sollten Sie kein Englisch sprechen, nutzen Sie bitte unseren Chat-Dienst.  Hierbei kommt ein automatisches Übersetzungstool zum Einsatz – die Qualität der Übersetzungen kann daher stark schwanken. ";
          break;
        case 'es-es':
          phoneMsg="Tenga en cuenta que la asistencia técnica únicamente está disponible <a href='#emeaPopDiv'>en inglés</a>. Si prefiere ponerse en contacto con nosotros por correo electrónico (solo en inglés), haga clic en “Send an Email”. ";
          emailMsg="Tenga en cuenta que la asistencia técnica únicamente está disponible <a href='#emeaPopDiv'>en inglés</a>. Proporcione una descripción detallada del problema técnico en inglés.";
          popMsg="Si no puede utilizar el inglés, acceda a nuestro servicio de chat.  Este servicio se basa en traducciones automáticas, por lo que la calidad de las traducciones puede variar. ";
          break;
        default:
          phoneMsg='Please note that technical support is provided in English only. If you would prefer to contact us via email (in English only), please click on "Send an Email".';
          emailMsg='Please note that technical support is provided in English only. Please provide a detailed description of your technical issue in English.';
          popMsg="If you are unable to use English, please use our Chat service.  This service is provided through automated machine translations, so please be aware that the quality of translations may vary. ";
      }      
      //$('#emeaMsgDiv').html(useMsg);
      var emeaMsgDiv2=$("<div />", {"class": 'msg', "id": 'emeaEmailMsgDiv'});      
      $('#btnContactSRF').closest('div').append(emeaMsgDiv2);
      $('#emeaPhoneMsgDiv').html(phoneMsg);
      $('#emeaEmailMsgDiv').html(emailMsg);
      
      var emeaPopDiv = $("<div />", {"id": 'emeaPopDiv', "class": 'pop' });
      emeaPopDiv.hide();
      $("div.col-md-12").append(emeaPopDiv);          
      $('#emeaPopDiv').html(popMsg);     
      
     
      $(".msg a").fancybox(
          {
            maxWidth : 800,
            fitToView  : true,
            autoSize  : true,
            closeClick  : false,
          }
        ); 
    }  //isContact page close
  }  //is EMEa close
  else if (isAnzLocale())
  {
        $(".hide-parentrow-in-anz").closest('div[class^="row"]').hide();
        $(".hide-parentcol-in-anz").closest('div[class^="col"]').hide();
        $(".hide-in-anz").hide();
    
        $(".show-parentrow-in-anz").closest('div[class^="row"]').show();
        $(".show-in-anz").show();
        $(".show-alert-in-anz").closest('div[class^="alert"]').show();
  }
  else if (getCurrentLocale()=='en-hk')
  {        
        $(".show-alert-in-hk").closest('div[class^="alert"]').show();
  }
      
});

  
function getEmeaCountry() {
 var emeaCountry = [];
  var curLocale=getCurrentLocale();  
  var deafultNum="+44 2035493382";
  var ukNum="+44 2035493381";
  var deNum="+49 81188990997";
  var frNum="+33 176686576";
  var itNum="+39 0292593400";
  var esNum="+34 913697128";  
 // var ob=new Object({locale: 'en-gb', contactNo: '911'});
  emeaCountry.push(new Object({locale: 'de-at', country: 'Austria', contactNo: deNum, abbre: 'at' }));    
  if(curLocale=="fr-be" || curLocale=="fr-fr") {
    emeaCountry.push(new Object({locale: 'fr-be', country: 'Belgium', contactNo: frNum, abbre: 'be' }));
  }  
  else {
    emeaCountry.push(new Object({locale: 'nl-be', country: 'Belgium', contactNo: deafultNum, abbre: 'be' }));
  }  
  emeaCountry.push(new Object({locale: 'bg-bg', country: 'Bulgaria', contactNo: deafultNum, abbre: 'bg' }));
  emeaCountry.push(new Object({locale: 'el-cy', country: 'Cyprus', contactNo: deafultNum, abbre: 'cy' }));
  emeaCountry.push(new Object({locale: 'cs-cz', country: 'Czech Republic', contactNo: deafultNum, abbre: 'cz' }));
  emeaCountry.push(new Object({locale: 'da-dk', country: 'Denmark', contactNo: deafultNum, abbre: 'dk' }));
  emeaCountry.push(new Object({locale: 'et-ee', country: 'Estonia', contactNo: deafultNum, abbre: 'ee' }));
  emeaCountry.push(new Object({locale: 'fi-fi', country: 'Finland', contactNo: deafultNum, abbre: 'fi' }));
  emeaCountry.push(new Object({locale: 'fr-fr', country: 'France', contactNo: frNum, abbre: 'fr' }));
  emeaCountry.push(new Object({locale: 'de-de', country: 'Germany', contactNo: deNum, abbre: 'de' }));
  emeaCountry.push(new Object({locale: 'el-gr', country: 'Greece', contactNo: deafultNum, abbre: 'gr' }));
  emeaCountry.push(new Object({locale: 'hu-hu', country: 'Hungary', contactNo: deafultNum, abbre: 'hu' }));
  emeaCountry.push(new Object({locale: 'en-ie', country: 'Ireland', contactNo: ukNum, abbre: 'ie' }));
  emeaCountry.push(new Object({locale: 'it-it', country: 'Italy', contactNo: itNum, abbre: 'it' }));
  emeaCountry.push(new Object({locale: 'lv-lv', country: 'Latvia', contactNo: deafultNum, abbre: 'lv' }));
  if(curLocale=="de-lu" || curLocale=="de-de") {
    emeaCountry.push(new Object({locale: 'de-lu', country: 'Luxembourg', contactNo: deNum, abbre: 'lu' }));
  }
  else if(curLocale=="fr-lu" || curLocale=="fr-fr") {
    emeaCountry.push(new Object({locale: 'fr-lu', country: 'Luxembourg', contactNo: frNum, abbre: 'lu' }));
  }
  else {
    emeaCountry.push(new Object({locale: 'de-lu', country: 'Luxembourg', contactNo: deafultNum, abbre: 'lu' }));
  } 
  emeaCountry.push(new Object({locale: 'nl-nl', country: 'Netherlands', contactNo: deafultNum, abbre: 'nl' }));
  emeaCountry.push(new Object({locale: 'nb-no', country: 'Norway', contactNo: deafultNum, abbre: 'no' }));
  emeaCountry.push(new Object({locale: 'pl-pl', country: 'Poland', contactNo: deafultNum, abbre: 'pl' }));
  emeaCountry.push(new Object({locale: 'pt-br', country: 'Portugal', contactNo: deafultNum, abbre: 'pt' }));
  emeaCountry.push(new Object({locale: 'ro-ro', country: 'Romania', contactNo: deafultNum, abbre: 'ro' }));
  emeaCountry.push(new Object({locale: 'ru-ru', country: 'Russian Federation', contactNo: deafultNum, abbre: 'ru' }));
  emeaCountry.push(new Object({locale: 'sl-si', country: 'Slovenia', contactNo: deafultNum, abbre: 'si' }));
  emeaCountry.push(new Object({locale: 'es-es', country: 'Spain', contactNo: esNum, abbre: 'es' }));
  emeaCountry.push(new Object({locale: 'sv-se', country: 'Sweden', contactNo: deafultNum, abbre: 'se' }));
  if(curLocale=="it-ch" || curLocale=="it-it") {
    emeaCountry.push(new Object({locale: 'it-ch', country: 'Switzerland', contactNo: itNum, abbre: 'ch' }));
  }
  else if(curLocale=="fr-ch" || curLocale=="fr-fr") {
    emeaCountry.push(new Object({locale: 'fr-ch', country: 'Switzerland', contactNo: frNum, abbre: 'ch' }));
  }
  else { // de and All
    emeaCountry.push(new Object({locale: 'de-ch', country: 'Switzerland', contactNo: deNum, abbre: 'ch' }));
  } 
  emeaCountry.push(new Object({locale: 'en-gb', country: 'United Kingdom', contactNo: ukNum, abbre: 'uk' }));
  emeaCountry.push(new Object({locale: 'ar-bh', country: 'Bahrain', contactNo: deafultNum, abbre: 'bh' }));
  emeaCountry.push(new Object({locale: 'he-il', country: 'Israel', contactNo: deafultNum, abbre: 'il' }));
  emeaCountry.push(new Object({locale: 'ar-sa', country: 'Saudi Arabia', contactNo: deafultNum, abbre: 'sa' }));
  emeaCountry.push(new Object({locale: 'tr-tr', country: 'Turkey', contactNo: deafultNum, abbre: 'tr' }));
  emeaCountry.push(new Object({locale: 'ar-ae', country: 'United Arab Emirates', contactNo: deafultNum, abbre: 'ae' }));
  emeaCountry.push(new Object({locale: 'en-za', country: 'South Africa', contactNo: deafultNum, abbre: 'za' }));
  return emeaCountry;
}
  
  function isEmeaLocale() {
    //var em=getEmeaCountry(); 
    //var _locale=$("#hdfLocale").val();
    //var i = em.length;
    //for(var j=0; j<i; j++) {
    //  if(em[j].locale==_locale) {
    //    return 1;
    //    break;
    //  }  
    //}  
    //return false;
    return $("#hdfIsEmea").val();   
    
  }
function isAnzLocale() {
  var curloc=$("#hdfLocale").val();
  if(curloc=='en-nz' || curloc=='en-au') return true;
  var hst = window.location.hostname.toLowerCase();
  if(hst.indexOf('.au') >= 0) return true;
  return false;
     
}

function getCurrentLocale() {
  return $("#hdfLocale").val();
}

function isContactPage() {
  var path=window.location.pathname.toLowerCase();
  if(path.indexOf('/contact.aspx') != -1 || 
     path.indexOf('/contact/home.aspx') != -1 ||
     path == '/en-us/home/pages/technical-support/contact' ||
     path == '/en-us/home/pages/technical-support/contact/') {
    return true;
  }
  return false;  
}
function noPhoneSupport() {
    var path=window.location.pathname.toLowerCase();
    if(path.indexOf('password-manager') >=0) return 1;
    if(path.indexOf('safesync') >=0) return 1;
    if(path.indexOf('mobile-security-for-android') >=0) return 1;
    if(path.indexOf('mobile-security-for-ios') >=0) return 1;       
    return false;
}
function isHhoIndexPage() {
  var j=window.location.pathname.toLowerCase().indexOf('/home/index.aspx');
  if(j>=0) return 1;
  else return false; 
}

function createEmeaLocaleOptions(selObjId) {
  
    var em=getEmeaCountry();
    var i = em.length;
    var opt;
    $('#' + selObjId).empty();
  
    
  
      opt = $("<option />", { "value": '' }).html('Select Country');
      $('#' + selObjId).append(opt)
        
      var optgroup = $('<optgroup>');
            optgroup.attr('label','Europe');
        $('#' + selObjId).append(optgroup)  
        
    for(var j=0; j<i; j++) {      
      if(em[j].country=='Bahrain') {
        optgroup = $('<optgroup>');
            optgroup.attr('label','Middle East');
        $('#' + selObjId).append(optgroup)      
      }
      else if(em[j].country=='South Africa') {
        optgroup = $('<optgroup>');
            optgroup.attr('label','South Africa');
        $('#' + selObjId).append(optgroup)      
      }    
          
          
      opt = $("<option />", { "value": em[j].locale }).html(em[j].country);
      $(optgroup).append(opt)
    }  

  }
  function getLocaleObject(_locale) {
    var em=getEmeaCountry();     
    var i = em.length;
    for(var j=0; j<i; j++) {
      if(em[j].locale==_locale) {
        return em[j];
        break;
      }  
    }  
    return false;    
  }
    
  function displayContactInfo(_locale) {
          if(_locale=="") {
            $("#emeaContactDiv").hide();            
            return;      
          }   
    
          var objLocale = getLocaleObject(_locale);
          if(objLocale==false) { // no locale found
            if(isEmeaLocale()) {
                $("#emeaContactDiv p.phone").html('+44 (0)1628 400 820');
                $("#emeaContactDiv").removeClass();          
                $("#emeaContactDiv").addClass('emea-number');
                $("#emeaContactDiv").show();
            }
            else {
              $("#emeaContactDiv").hide();
              $("#selLocale").val('');
            }
            return;
          }
          $("#curCountry").html(objLocale.country);
          var arr= objLocale.contactNo.split('@');  
          if(arr.length > 1) {
            var l = arr.length;
            var html='';
            for(i=0;i<l;i++) {
              html += arr[i] + '</br>';
            }   
            $("#emeaContactDiv p.phone").html(html);
          }
          else {
            $("#emeaContactDiv p.phone").html(objLocale.contactNo)            
          }
            
          $("#emeaContactDiv").removeClass();           

          $("#emeaContactDiv").addClass(objLocale.abbre); 
          $("#emeaContactDiv").addClass('emea-number');  
          $("#emeaContactDiv").show();
  }
    
    function updateGlobalNav() {
    var objLinks = new Object({home:'',smb:'',ent:'',secRep:'',whyTm:'',supHome:'/en-us/home/index.aspx',supBuss:'/en-us/business/default.aspx'});
      var loc=getCurrentLocale();   
    
      switch(loc) {
        case "da-dk":
           objLinks.home='http://www.trendmicro.dk/home/index.html';
           objLinks.smb='http://www.trendmicro.dk/small-business/index.html';
           objLinks.ent='http://www.trendmicro.dk/enterprise/index.html';
           objLinks.secRep='http://www.trendmicro.dk/security-intelligence/index.html';
           objLinks.whyTm='http://www.trendmicro.dk/technology-innovation/index.html';
           //objLinks.supHome='';
           //objLinks.supBuss='';
           break;
        case "de-de":
           objLinks.home='http://www.trendmicro.de/privatanwender/index.html';
           objLinks.smb='http://www.trendmicro.de/kleinunternehmen/virus-und-bedrohungsschutz/index.html';
           objLinks.ent='http://www.trendmicro.de/grossunternehmen/index.html';
           objLinks.secRep='http://www.trendmicro.de/sicherheitsinformationen/index.html';
           objLinks.whyTm='http://www.trendmicro.de/technologie-innovationen/index.html';           
           break;
        case "es-es":
            objLinks.home='http://www.trendmicro.fr/particuliers/index.html';
           objLinks.smb='http://www.trendmicro.es/pequenas-empresas/proteccion-frente-a-virus-y-amenazas/';
           objLinks.ent='http://www.trendmicro.es/grandes-empresas/index.html';
           objLinks.secRep='http://www.trendmicro.es/informacion-seguridad/index.html';
           objLinks.whyTm='http://www.trendmicro.es/tecnologia-innovacion/index.html';           
           break;
        case "fr-fr":
           objLinks.home='http://www.trendmicro.fr/particuliers/index.html';
           objLinks.smb='http://www.trendmicro.fr/petites-entreprises/index.html';
           objLinks.ent='http://www.trendmicro.fr/grandes-entreprises/index.html';
           objLinks.secRep='http://www.trendmicro.fr/renseignements-securite/index.html';
           objLinks.whyTm='http://www.trendmicro.fr/technologie-innovation/index.html';           
           break;
        case "it-it":
          objLinks.home='http://www.trendmicro.it/privati/index.html';
           objLinks.smb='http://www.trendmicro.it/piccole-imprese/index.html';
           objLinks.ent='http://www.trendmicro.it/aziende-di-grandi-dimensioni/index.html';
           objLinks.secRep='http://www.trendmicro.it/informazioni-sulla-sicurezza/index.html';
           objLinks.whyTm='http://www.trendmicro.it/tecnologia-innovazione/index.html';           
           break;
        case "nl-nl":
           objLinks.home='http://www.trendmicro.nl/thuis/index.html';
           objLinks.smb='http://www.trendmicro.nl/kleine-ondernemingen/index.html';
           objLinks.ent='http://www.trendmicro.nl/grote-ondernemingen/index.html';
           objLinks.secRep='http://www.trendmicro.nl/beveiligingskennis/index.html';
           objLinks.whyTm='http://www.trendmicro.nl/technologie-innovatie/index.html';           
           break;
        case "nb-no":
          objLinks.home='http://www.trendmicro.no/home/index.html';
           objLinks.smb='http://www.trendmicro.no/small-business/index.html';
           objLinks.ent='http://www.trendmicro.no/enterprise/index.html';
           objLinks.secRep='http://www.trendmicro.no/security-intelligence/index.html';
           objLinks.whyTm='http://www.trendmicro.no/technology-innovation/index.html';           
           break;          
        case "ru-ru":
           objLinks.home='http://www.trendmicro.com.ru/home/index.html';
           objLinks.smb='http://www.trendmicro.com.ru/small-business/index.html';
           objLinks.ent='http://www.trendmicro.com.ru/enterprise/index.html';
           objLinks.secRep='http://www.trendmicro.com.ru/security-intelligence/index.html';
           objLinks.whyTm='http://www.trendmicro.com.ru/technology-innovation/index.html';
           objLinks.supHome='';
           objLinks.supBuss='';
           break;
        case "sv-se":
          objLinks.home='http://www.trendmicro.se/home/index.html';
           objLinks.smb='http://www.trendmicro.se/small-business/virus-and-threat-protection/';
           objLinks.ent='http://www.trendmicro.se/enterprise/index.html';
           objLinks.secRep='http://www.trendmicro.se/security-intelligence/index.html';
           objLinks.whyTm='http://www.trendmicro.se/technology-innovation/index.html';          
           break;
        case "tr-tr":
           objLinks.home='http://www.trendmicro.com.tr/ev/index.html';
           objLinks.smb='http://www.trendmicro.com.tr/kucuk-isletme/virus-ve-tehditlerden-koruma/';
           objLinks.ent='http://www.trendmicro.com.tr/kurum/index.html';
           objLinks.secRep='http://www.trendmicro.com.tr/guvenlik-istihbarati/index.html';
           objLinks.whyTm='http://www.trendmicro.com.tr/teknoloji-yenilik/index.html';
           objLinks.supHome='';
           objLinks.supBuss='';
           break;
        case "en-ie":
           objLinks.home='http://www.trendmicro.ie/home/index.html';
           objLinks.smb='http://www.trendmicro.ie/small-business/virus-and-threat-protection/';
           objLinks.ent='http://www.trendmicro.ie/enterprise/index.html';
           objLinks.secRep='http://www.trendmicro.ie/security-intelligence/index.html';
           objLinks.whyTm='http://www.trendmicro.ie/technology-innovation/index.html';           
          break;
        default: //use en-gb
           objLinks.home='http://www.trendmicro.co.uk/home/index.html';
           objLinks.smb='http://www.trendmicro.co.uk/small-business/virus-and-threat-protection/index.html';
           objLinks.ent='http://www.trendmicro.co.uk/enterprise/index.html';
           objLinks.secRep='http://www.trendmicro.co.uk/security-intelligence/index.html';
           objLinks.whyTm='http://www.trendmicro.co.uk/technology-innovation/index.html';           
      }
    
    $('ul.ti-global-nav-menu li a, #techsupport li a').each(function(){
        var menuName = $(this).text();
        switch(menuName) {
          case "For Home":
            $(this).attr("href",objLinks.home);
            break;
          case "For Small Business":
            $(this).attr("href",objLinks.smb);
            break;
          case "For Enterprise & Midsize Business":
            $(this).attr("href",objLinks.ent);
            break;
          case "Security Report":
            $(this).attr("href",objLinks.secRep);
            break;
          case "Why Trendmicro":
            $(this).attr("href",objLinks.whyTm);
            break;
          case "Home & Home Office Support":
            $(this).attr("href",objLinks.supHome);
            break;
          case "Business Support":
            $(this).attr("href",objLinks.supBuss);
            break;
        }

    });
    
  }

  

